<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cookie;

class ExamController extends Controller
{
	public function index()
	{
		// mengambil data soal yang telah di input 
		$stored_question = Cookie::get('stored_question');
		// cek apakah ada soal yang telah di input
		if ($stored_question!=null) {
			$stored_question = json_decode($stored_question,false);
		}else{
			$stored_question = [];
		}
		return view('exam.index',compact('stored_question'));
	}

	public function store(Request $request)
	{
		// mengabil soal yang telah di input

		$stored_question = Cookie::get('stored_question');
		// cek apakah ada soal yang telah di input
		if ($stored_question==null) {
			// inisialisasi soal baru
			$new_question = array(
				'question' => $request->question, 
				'option_a' => $request->option_a, 
				'option_b' => $request->option_b, 
				'option_c' => $request->option_c, 
				'option_d' => $request->option_d, 
				'answer'   => $request->answer,
			);
			$stored_question = [];
			array_push($stored_question, $new_question);
			$stored_question = json_encode($stored_question);
			// menyimpan soal
			Cookie::queue('stored_question',$stored_question);
			return back()->with('success','Question successfully sotred');
		}else{
			$stored_question = json_decode($stored_question,false);
			// inisialisasi soal baru
			$new_question = array(
				'question' => $request->question, 
				'option_a' => $request->option_a, 
				'option_b' => $request->option_b, 
				'option_c' => $request->option_c, 
				'option_d' => $request->option_d, 
				'answer'   => $request->answer,
			);
			array_push($stored_question,$new_question);
			$stored_question = json_encode($stored_question);
			// menyimpan soal
			Cookie::queue('stored_question',$stored_question);
			return back()->with('success','Question successfully sotred');
		}
	}

	public function reset()
	{
		Cookie::queue('stored_question',null);
		Cookie::queue('time_remaining',null);
		Cookie::queue('exam_data',null);
		Cookie::queue('answered',null);
		return redirect('')->with('success','Reset success!');
	}

	public function start_exam(Request $request)
	{
		// cek apakah ada soal yang telah di input
		if (Cookie::get('stored_question')==null) {
			return back()->with('error','You have to input at least one question to start the exam!');
		}
		// cek waktu yang di input
		if ($request->time<=0) {
			return back()->with('error','Minimal limit time is 1 minute');
		}
		// inisialisasi data ujian
		$exam_data = array(
			'time' 		=> 	$request->time,
			'is_done'	=>	false,
			'current_q'	=>  0 
		);
		$exam_data = json_encode($exam_data);
		Cookie::queue('exam_data',$exam_data);
		return redirect('exam');
	}

	public function exam()
	{
		// Cookie::queue('exam_data',null);
		$exam_data = Cookie::get('exam_data');
		$exam_data = json_decode($exam_data,true);
		return view('exam.do_exam',compact('exam_data'));
	}

	public function get_question($index)
	{
		$list_question = Cookie::get('stored_question');
		$list_question = json_decode($list_question,false);		
		$answered = Cookie::get('answered');
		$answered = json_decode($answered,true);
		if ($answered!=null) {
			foreach ($answered as $key) {
				if ($key['index']==$index) {
					$answered = $key['answer'];
				}
			}
		}else{
			$answered = '-';
		}

		$respon = array(
			'question' 		=> $list_question[$index],
			'answered'	=> $answered 
		);
		return $respon;
	}


	public function set_time($minute)
	{
		$time_remaining = $minute;
		if (Cookie::get('time_remaining')==null) {
			Cookie::queue('time_remaining',$minute);
		}else{
			Cookie::queue('time_remaining',$minute);
			$time_remaining = Cookie::get('time_remaining');
		}
		
		$answered = "";
		if (Cookie::get('answered')!=null) {
			$answered = Cookie::get('answered');
		}
		$respon = array(
			'answered' => $answered,
			'time_remaining' => (int)$time_remaining, 
		);
		return json_encode($respon);
	}

	public function answer($index,$opt)
	{
		// Cookie::queue('answered',null);
		$answered = Cookie::get('answered');
		if ($answered==null) {
			$new_arr_jawaban = [];
			$new_tmp_jawaban = array(
				'index' => $index,
				'answer' => $opt 
			);

			array_push($new_arr_jawaban, $new_tmp_jawaban);
			$arr = json_encode($new_arr_jawaban);
			Cookie::queue('answered',$arr);
			return $arr;
		}else{
			$answered = json_decode($answered,true);
			$is_there = 0;
			$no = 0;
			foreach ($answered as $key) {
				if ($index==$key['index']) {
					$is_there++;
					unset($answered[$no]);
				}
				$no++;
			}
			$answered = array_values($answered);
			$new_tmp_jawaban = array(
				'index' => $index,
				'answer' => $opt 
			);
			// $new_tmp_jawaban = array_values($new_tmp_jawaban);
			array_push($answered,$new_tmp_jawaban);
			$arr = json_encode($answered);
			Cookie::queue('answered',$arr);
			return $arr;
		}
	}

	public function done()
	{
		$exam_data = Cookie::get('exam_data');
		$exam_data = json_decode($exam_data,true);
		$exam_data['is_done'] = true;
		$exam_data = json_encode($exam_data);
		Cookie::queue('exam_data',$exam_data);
		return "lorem";
	}

	public function analysis()
	{
		if (Cookie::get('exam_data')==null) {
			return redirect('')->with('error','Please input the questions first lah bodoo!');
		}
		return view('exam.analysis');
	}
}


// ket : decode => string->object
//		 encode => object->string