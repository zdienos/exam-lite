@extends('layout.exam_layout')
@section('title','Exam-Lite | Do Exam')
@section('content')
<div class="content">
	<div class="panel-header bg-primary-gradient">
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
				<div>
					<h2 class="text-white pb-2 fw-bold">Exam Lite</h2>
					<h5 class="text-white op-7 mb-2">Practice makes us right, repetitions make us perfect.</h5>
				</div>
				<div class="ml-md-auto py-2 py-md-0">
					<button type="button" class="btn btn-white btn-border btn-round mr-2 btn-prev"onclick="prev()">Prevtious</button>
					<button type="button" class="btn btn-white btn-border btn-round mr-2 btn-next"onclick="next()">Next</button>
					<button type="button" class="btn btn-secondary btn-round btn-done hide" onclick="conf_done()">Finish the Exam</button>
				</div>
			</div>
		</div>
	</div>
	<div class="page-inner mt--5">
		<div class="row mt--2">
			<div class="col-md-12">
				<div class="card full-height">
					<div class="card-header">
						<b>Question Number : <span class="badge badge-info" id="question_num">01</span></b>
						<b class="float-right">
							remaining time : 
							<span id="timer" class="col-12">
								<div class="clock-wrapper badge badge-info">
									<span class="hours">00</span>
									<span class="dots">:</span>
									<span class="minutes">00</span>
									<span class="dots">:</span>
									<span class="seconds">00</span>
								</div>
							</span>
						</b>
					</div>
					<div class="card-body">
						<div style="min-height: 300px;">
							<div style="width: 100%;" id="question_container">
							</div>       
							<br>
							<ol type="A" id="option" style="margin-left: -20px;">

							</ol>
						</div>
						<button class="btn  btn-option" id="btn_a" onclick="answer('A')" style="border-radius: 50px;">A</button>
						<button class="btn  btn-option" id="btn_b" onclick="answer('B')" style="border-radius: 50px;">B</button>
						<button class="btn  btn-option" id="btn_c" onclick="answer('C')" style="border-radius: 50px;">C</button>
						<button class="btn  btn-option" id="btn_d" onclick="answer('D')" style="border-radius: 50px;">D</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="custom-template">
	<div class="title">List Soal</div>
	<div class="custom-content">
		<div class="switcher">
			<div class="switch-block" style="min-height: 400px;">
				@php
				$answered = Cookie::get('answered');
				$answered = json_decode($answered,true);

				$list_question = Cookie::get('stored_question');
				$list_question = json_decode($list_question,false);
				@endphp
				@foreach ($list_question as $e)
				@php
				$class = "";

				$class = "";
				$opt  = "";
				if ($answered!=null) {
					foreach ($answered as $key) {
						if ($key['index']==$loop->index) {
							$class = "btn-success";
							$opt = $key['answer'];
						}
					}
				}
				@endphp
				@if ($loop->index==1)
				<button onclick="get_question({{ $loop->index }})" id="btn_q_{{ $loop->index }}" class="btn {{ $class=="" ? "btn-info" : "btn-success"  }}">{{ $loop->index + 1 }}</button>
				<span id="container-option-{{ $loop->index }}">
					@if ($opt!="")
					<span style="position: absolute;background-color: #ffc107;color:white;margin-left: -20px;padding: 0px 8px; margin-top: -10px;border-radius: 30px;font-size: 18px;">{{ $opt }}</span>
					@endif
				</span>
				@else
				<button onclick="get_question({{ $loop->index }})" id="btn_q_{{ $loop->index }}" class="btn {{ $class=="" ? "btn-default" : "btn-success"  }}">{{ $loop->index + 1 }}</button>
				<span id="container-option-{{ $loop->index }}">
					@if ($opt!="")
					<span style="position: absolute;background-color: #ffc107;color:white;margin-left: -20px;padding: 0px 8px; margin-top: -10px;border-radius: 30px;font-size: 18px;">{{ $opt }}</span>
					@endif
				</span>
				@endif
				@endforeach

                <div class="col-sm-12" style="position: absolute;bottom: 0">
                    <center>

                        <ul class="nav nav-pills nav-secondary nav-pills-no-bd nav-pills-icons justify-content-center" id="pills-tab-with-icon" role="tablist">
                            <li class="nav-item" style="width: 80px">
                                <a class="nav-link" target="__blank" href="http://lumos-indo.com/" role="tab" aria-controls="pills-home-icon" aria-selected="true">
                                    <i class="fas fa-globe"></i>
                                </a>
                            </li>
                            <li class="nav-item" style="width: 80px">
                                <a class="nav-link" target="__blank" href="https://www.instagram.com/firmanpraa_/" role="tab" aria-controls="pills-profile-icon" aria-selected="false">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li class="nav-item" style="width: 80px">
                                <a class="nav-link" target="__blank" href="https://api.whatsapp.com/send?phone=6283811212847&text=Halo mas tampan" role="tab" aria-controls="pills-contact-icon" aria-selected="false">
                                    <i class="fab fa-whatsapp"></i>
                                </a>
                            </li>
                        </ul>

                    </center>
                </div>
            </div>
        </div>
    </div>

    <div class="custom-toggle">
      <i class="fa fa-arrow-left" style="animation: none!important;"></i>
  </div>
</div>
@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/countdown/css/style.css') }}"> 
<style>
	.hide {
		display: none;
	}
</style>
@endsection

@section('js')
<script type="text/javascript" src="{{ asset('assets/countdown/js/script.js') }}"></script> 
<script src="{{ asset('assets/sweetalert/sweetalert.min.js') }}"></script>

<script>
	const s = $(timer).find('.seconds')
	const m = $(timer).find('.minutes')
	const h = $(timer).find('.hours')

	var id_curr_soal = 0;
	var curr_index_q = 0;
	var seconds = 0
	var minutes = 0
	var store_minutes = 0
	var hours = 0

	var interval = null;

	var clockType = undefined;

	var json_anwered = "";

	function pad(d) {
		return (d < 10) ? '0' + d.toString() : d.toString()
	}

        // function cunstruct
        startClock();
        get_question(0);

        // inisialisasi timer
        function startClock() {
        	iziToast.info({
        		title: 'Info',
        		message: "Do your best to get the best!",
        		position: 'topRight'
        	});

        	hasStarted = false
        	hasEnded = false

        	seconds = 0
        	minutes = 0
        	hours = 0
            // min_start = 110;
            @if (Cookie::get('time_remaining')==null)
            min_start = {{ $exam_data['time'] }};
            @else
            min_start = {{ Cookie::get('time_remaining') }};
            @endif
            // alert(min_start);
            store_minutes = min_start;
            console.log(min_start);
            switch ('m') {
            	case 'm':
            	if (min_start > 59) {
            		let hou = Math.floor(min_start / 60)
            		hours = hou
            		let min = min_start - (hou * 60)
            		minutes = min
            	}
            	else {
            		minutes = min_start
            	}
            	break
            	default:
            	break
            }

            if (seconds <= 10  && minutes == 0 && hours == 0) {
            	$(timer).find('span').addClass('red')
            	$(".clock-wrapper").removeClass('badge-info');
            	$(".clock-wrapper").addClass('badge-danger');
            }

            if (seconds <= 10  && minutes == 0 && hours == 0) {
            	$(timer).find('span').addClass('red')
            	$(".clock-wrapper").removeClass('badge-info');
            	$(".clock-wrapper").addClass('badge-danger');
            }
            

            refreshClock()

            $('.input-wrapper').slideUp(350)
            setTimeout(function(){
            	$('#timer').fadeIn(350)
            	$('#stop-timer').fadeIn(350)

            }, 350)

            countdown()
        }

        var hasStarted = false
        var hasEnded = false
        if (hours == 0 && minutes == 0 && seconds == 0 && hasStarted == true) {
        	hasEnded = true
        }

        // fungsi timer
        function countdown() {
        	hasStarted = true
        	interval = setInterval(() => {
        		if(hasEnded == false) {
        			if (seconds <= 11 && minutes == 0 && hours == 0) {
        				$(timer).find('span').addClass('red')
        				$(".clock-wrapper").removeClass('badge-info')
        				$(".clock-wrapper").addClass('badge-danger')
        			}

        			if (minutes == 9 && hours == 0 && seconds == 59) {
        				iziToast.warning({
        					title: 'Info',
        					message: 'Waktu kamu tinggal 10 menit lagi',
        					position: 'topRight'
        				});
        			}


        			if(seconds == 0 && minutes == 0 || (hours > 0  && minutes == 0 && seconds == 0)) {
        				hours--
        				minutes = 59
        				seconds = 60
        				refreshClock()
        			}

        			if(seconds > 0) {
        				seconds--
        				refreshClock()
        			}
        			else if (seconds == 0) {
        				minutes--
        				store_minutes--
        				seconds = 59
        				refreshClock()
        			}
        		}
        		else {
        			restartClock()
        		}
                // alert(minutes);
                set_waktu(parseInt(store_minutes))
            }, 1000)
        }

        // set waktu berdasar cookie
        function set_waktu(menit) {
        	$.ajax({
        		type: 'GET', 
        		url: '{{ url('exam/set-time/') }}/'+menit,
                // typeData : "JSON",
                success: function (data) {
                	data = JSON.parse(data);
                	json_anwered = data.answered;
                    // console.log(data);
                    // console.log('{{ url('exam/set-time/') }}/'+menit);
                    is_done();
                },error:function(){
                	console.log(data);
                }
            });
        }

        function refreshClock() {
        	$(s).text(pad(seconds))
        	$(m).text(pad(minutes))
        	if (hours < 0) {
        		$(s).text('00')
        		$(m).text('00')
        		$(h).text('00')
        	} else {
        		$(h).text(pad(hours))
        	}

        	if (hours == 0 && minutes == 0 && seconds == 0 ) {
        		hasEnded = true
                // alert('The Timer has Ended !')
                clear(interval)
            }
        }

        // clear timer
        function clear(intervalID) {
        	clearInterval(intervalID)
        	console.log('cleared the interval called ' + intervalID)
        }
        
        // fungsi mengambil soal berdasar id 
        function get_question(index) {
        	total_q = {{ count($list_question) }};

        	if (index==0) {
        		$(".btn-prev").addClass('hide');
        		$(".btn-next").removeClass('hide');
        	}else if(index==total_q-1){
        		$(".btn-prev").removeClass('hide');
        		$(".btn-next").addClass('hide');
        	}else{
        		$(".btn-prev").removeClass('hide');
        		$(".btn-next").removeClass('hide');
        	}

        	id_curr_soal = index;
        	curr_index_q = index;
        	$.ajax({
        		type: 'GET', 
        		url: '{{ url('exam/get-question/') }}/'+index,
        		success: function (data) {
        			console.log(data);
        			$("#question_container").html('');
        			$("#option").html('');
        			$("#question_num").html(index + 1);
        			$("#question_container").html(data.question.question)
        			$("#option").append('<li>'+data.question.option_a+'</li>');
        			$("#option").append('<li>'+data.question.option_b+'</li>');
        			$("#option").append('<li>'+data.question.option_c+'</li>');
        			$("#option").append('<li>'+data.question.option_d+'</li>');
        			$(".btn-info").removeClass('btn-info');
        			$("#btn_q_"+index).addClass('btn-info');

        			if (data.answered!="-") {
        				$(".btn-option").removeClass('btn-info');
        				$("#btn_"+data.answered.toLowerCase()).removeClass('');
        				$("#btn_"+data.answered.toLowerCase()).addClass('btn-info');
        				$("#btn_q_"+index).addClass('btn-success');
        			}
        		},error:function(){
        			console.log(data);
        		}
        	});
        }

        function prev() {
        	get_question(curr_index_q - 1);
        }

        function next() {
        	get_question(curr_index_q + 1);
        }


        function answer(opt) {
        	$.ajax({
        		type: 'GET', 
        		url: '{{ url('exam/answer/') }}/'+curr_index_q+"/"+opt,
        		success: function (data) {
        			console.log(data);
        			$(".btn-option").removeClass('btn-info');
        			$("#btn_"+opt.toLowerCase()).removeClass('btn-default');
        			$("#btn_"+opt.toLowerCase()).addClass('btn-info');
        			$("#container-option-"+curr_index_q).html('');
        			$("#container-option-"+curr_index_q).append('<span style="position: absolute;background-color: #ffc107;color:white;margin-left: -20px;padding: 0px 8px; margin-top: -10px;border-radius: 30px;font-size: 18px;">'+opt+'</span>');
        			$("#btn_q_"+curr_index_q).addClass('btn-success');
        		},error:function(){
        			console.log(data);
        		}
        	});
        	is_done();
        }

        function is_done() {
        	var question_num  = {{ count($list_question) }};
        	if (json_anwered!=null || json_anwered!="" ) {
        		var answered = json_anwered;
        		answered = JSON.parse(answered);
        		answered = answered.length
        	}else{
        		var answered = 0;
        	}
        	if (question_num ==answered) {
        		$(".btn-done").removeClass('hide');
        	}else{
        		$(".btn-done").addClass('hide');
        	}
        }

        function conf_done() {
        	var question_num = {{ count($list_question) }};
        	if (json_anwered!=null || json_anwered!="" ) {
        		var answered = json_anwered;
        		answered = JSON.parse(answered);
        		answered = answered.length

        	}else{
        		var answered = 0;
        	}

        	if (question_num==answered) {
        		swal({
        			title: 'Finish exam?',
        			text: "Your answer will not be able to be changed after you click 'Ok'",
        			icon: 'warning',
        			buttons: true,
        			confirmButtonClass: "btn-success",
        			confirmButtonText: "Yes, delete it!",
        		})
        		.then((willDelete) => {
        			if (willDelete) {
                        // $("#form-"+id).submit();
                        done();
                    }
                });
        	}
        }

        function done() {
        	$.ajax({
        		type: 'GET', 
        		url: '{{ url('exam/done/') }}',
        		success: function (data) {
        			window.location.href="{{ url('exam') }}";
        		},error:function(){
        			console.log(data);
        		}
        	});
        }

    </script>
    @endsection