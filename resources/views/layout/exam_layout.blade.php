
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	{{-- agar title bisa di set di file yang menginduk ke file index_layout.blade.php  --}}
	<title>@yield('title')</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="../../assets/img/icon.ico" type="image/x-icon"/>
	
	<!-- Fonts and icons -->
	<script src="{{ asset('assets/js/plugin/webfont/webfont.min.js') }}"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../../assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/atlantis.min.css') }}">
	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}">
	{{-- yield css agar kita bisa memasukan css di file yang menginduk ke file index_layout.blade.php dan di tempatkan di line terbawah --}}
	@yield('css')
</head>
<body>
	<div class="wrapper">
		<div class="main-panels">
			@yield('content')
			<footer class="footer">
				<div class="container-fluid">
					<div class="copyright ml-auto">
						Theme by <a href="https://www.themekita.com">ThemeKita</a>
					</div>				
					<nav class="pull-right">
						<ul class="nav">
							<li class="nav-item">
								<a class="nav-link" href="http://lumos-indo.com/">
									Lumosindo
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="https://api.whatsapp.com/send?phone=6283811212847&text=Halo mas tampan">
									WhatsApp
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="https://www.instagram.com/firmanpraa_/">
									Instagram
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</footer>
		</div>
	</div>
	
	<!--   Core JS Files   -->
	<script src="{{ asset('assets/js/core/jquery.3.2.1.min.js') }}"></script>
	<script src="{{ asset('assets/js/core/popper.min.js') }}"></script>
	<script src="{{ asset('assets/js/core/bootstrap.min.js') }}"></script>
	<!-- jQuery UI -->
	<script src="{{ asset('assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>
	
	<!-- jQuery Scrollbar -->
	<script src="{{ asset('assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
	<!-- Atlantis JS -->
	<script src="{{ asset('assets/js/atlantis.min.js') }}"></script>
	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script src="{{ asset('assets/js/setting-demo2.js') }}"></script>
	{{-- memasukan fungsi alert --}}
	@include('alert')
	{{-- yield js agar kita bisa memasukan js di file yang menginduk ke file index_layout.blade.php dan di tempatkan di line terbawah --}}
	@yield('js')
</body>
</html>