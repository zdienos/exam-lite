<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ExamController@index')->middleware('question');

Route::group(['prefix' => 'question', 'middleware' => 'question'], function () {
	Route::get('/','ExamController@index');

	// store new question
	Route::post('store','ExamController@store');
	Route::post('start-exam','ExamController@start_exam');
});
Route::get('reset','ExamController@reset');

Route::group(['prefix' => 'exam', 'middleware' => 'exam'], function () {
	Route::get('/','ExamController@exam');
	Route::get('set-time/{time}','ExamController@set_time');
	Route::get('get-question/{index}','ExamController@get_question');
	Route::get('answer/{index}/{opt}','ExamController@answer');
	Route::get('done','ExamController@done');
});

Route::get('analysis','ExamController@analysis');